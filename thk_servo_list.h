/*
 * Klasse: ServoList
 *
 * Diese Klasse kann bis zu 8 servo_controller als Liste halten. Die Methode process()
 * jedes servo_controllers wird dann durch Rufen der Methode process() der ServoList 
 * ausgeführt
 * 
 * 
 */

#ifndef SERVO_LIST_H
#define SERVO_LIST_H

#include <thk_servo_controller.h>
#define MAXSERVOS 8//     <<<------HIER MAX SERVOS EINSTELLEN!!!

class thk_ServoList
{
  public:
    void addServo(uint8_t aPin, uint32_t aStepTime, int16_t aPos);
    void addServo(uint8_t aPin, uint32_t aStepTime);
    void calibrateServo(uint8_t servo_no, int16_t aLeft, int16_t aMid, int16_t aRight);
    void write(uint8_t servo_no, int16_t aPos);
    void write_for_process(uint8_t servo_no, int16_t aPos);
    void process();
  private:
    static uint8_t countvalue;
    thk_ServoController* servos[MAXSERVOS];
};

#endif