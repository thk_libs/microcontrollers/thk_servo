#include "thk_servo_controller.h"

thk_ServoController::thk_ServoController(uint8_t aPin)
: pin(aPin)
{

};

thk_ServoController::thk_ServoController(uint8_t aPin, uint32_t aStepTime)
: pin(aPin), stepTime(aStepTime)
{

};

thk_ServoController::thk_ServoController(uint8_t aPin, uint32_t aStepTime, int16_t aPos)
: pin(aPin), stepTime(aStepTime), pos(aPos)
{

};

thk_ServoController::thk_ServoController(uint8_t aPin, int16_t aPos, int16_t aMaxAngle, int16_t aMinAngle)
: pin(aPin), pos(aPos), maxAngle(aMaxAngle), minAngle(aMinAngle)
{

};

void thk_ServoController::begin()
{
  servo.attach(pin);
  servo.write(pos);
}

void thk_ServoController::end()
{
  servo.detach();
}

void thk_ServoController::find_max_angle(Stream &port, uint8_t _min, uint8_t _max)
{
  static bool x = false;

  while (x == false)
  {
    #ifdef WIFI_COMMUNICATION
    _println_("-------------------------");
    _println_("Manuelle Bereichsfindung des Servomotors auf PIN:");
    _println_(pin);
    _println_("Gebe einen Winkel zur Steuerung des Servomotors ein.");
    _println_("Gültig sind Werte im Bereich 0 - 180 ");
    _println_("-------------------------");
    #else
    port.println(F("-------------------------"));
    port.print(F("Manuelle Bereichsfindung des Servomotors auf PIN:"));
    port.println(pin);
    port.println(F("Gebe einen Winkel zur Steuerung des Servomotors ein."));
    port.print(F("Gueltig sind Werte im Bereich:"));
    port.print(_min);
    port.print(F(" - "));
    port.println(_max);
    port.println(F("-------------------------"));
    #endif
    
    x = true;
  }

  if (port.available())
  {
    int pos = port.parseInt();

    if (pos < _min || pos > _max)
    {
      port.println(F("Nur Werte im angegebenen Wertebereich"));
      port.read();
    }
    else if (pos >= _min && pos <= _max)
    {
      port.print(F("Auf "));
      port.print(pos);
      port.println(F(" Grad gestellt"));
      write(pos);
      port.read();
    }
  }
}

void thk_ServoController::write(int16_t aPos)
{
  if (aPos < 90)
    servo.write(map(aPos, 0, 90, left, mid));
  else if (aPos > 90)
    servo.write(map(aPos, 90, 180, mid, right));
  else if (aPos == 90)
    servo.write(mid);
}

void thk_ServoController::write_for_process(int16_t target_pos)
{
  pa = pos;
  pe = target_pos;
  ta = millis();
  te = ta + stepTime;
  m  = (float)(pe - pa) / (float)stepTime;

  isBusy = 1;
}

void thk_ServoController::turn(int16_t aPos)
{
  if (aPos > maxAngle){
    aPos = maxAngle;
  }
  else if (aPos < minAngle){
    aPos = minAngle;
  }

  write(aPos);
}

void thk_ServoController::process()
{
  if (!isBusy) return;
  if ((pe != pos) && (millis() < te))
  {
    pos =int( m * (millis() - ta) + float(pa) );
    servo.write(pos);
  }
  else if (millis() > te)
    isBusy = 0;
}
