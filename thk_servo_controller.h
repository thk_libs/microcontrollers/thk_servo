/*
 * Klasse: servo_controller.h
 * 
 * Diese Klasse hat eine zusätzliche Funktion zur Servosteuerung. 
 * Der Servo wird in Schritten (steps) zu seiner Zielposition gefahren.
 * 
 */

#ifndef SERVO_CONTROLLER_H
#define SERVO_CONTROLLER_H

#include <Arduino.h>
#include <Servo.h>


class thk_ServoController
{
  public:
    thk_ServoController(uint8_t aPin);   
    thk_ServoController(uint8_t aPin, uint32_t aStepTime);
    thk_ServoController(uint8_t aPin, uint32_t aStepTime, int16_t aPos);
    thk_ServoController(uint8_t aPin, int16_t aPos, int16_t aMaxAngle, int16_t aMinAngle);
    void begin();
    void end();
    void find_max_angle(Stream &port = Serial, uint8_t _min = 0, uint8_t _max = 180);
    void calibrate(int16_t aLeft, int16_t aMid, int16_t aRight){mid=aMid;right=aRight;left=aLeft;};
    void write(int16_t aPos);
    void write_for_process(int16_t target_pos);
    void turn(int16_t aPos);
    uint8_t isBusy;   
    void process();
    
  private:
    Servo servo;          // Servo-Instanz
    uint8_t pin;          // PWM-Pin für Servo
    uint16_t stepTime=0;  // Zeit für eine Bewegung
    int16_t pos, pa, pe, maxAngle, minAngle;  // pos = aktuelle Position
                                              // pa = Startposition einer Bewegung
                                              // pe = Endposition einer Bewegung
                                              // maxAngle = maximaler Winkel
                                              // minAngle = minimaler Winkel
    int16_t left=0, mid=90, right=180;        // Linker, rechter Anschlag und Mittenposition
    uint32_t ta, te;                          // ta = Zeit bei Start der Bewegung
                                              // te = Zeit bei Ende der Bewegung
    float m;                                  // Steigung, d.h. Delta_Winkel/Zeit
};

#endif
