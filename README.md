# **thk_ServoController**

Diese Klasse hat zusätzliche Funktionen für die Servosteuerung und soll die Funktionen der Servo.h-Library etwas erweitern:
 * Ein thk_ServoController kann eine Zielstellung in einer definierten Zeit erreichen.
 * Der Servo wird dann mit konstanter Geschwindigkeit zu seiner Zielposition gefahren.
 * Die Zeit für eine Bewegung (=Step) wird beim Erzeugen übergeben und als "stepTime" gespeichert.
 * process() muss dazu im loop kontinuierlich gerufen werden.
 * Es können Grenzen bei der Instanziierung direkt mitübergeben werden, die der thk_ServoController einhält.
 * Natürlich kann der Servo nicht schneller als mit seiner maximalen Rotationsgeschwindigkeit gefahren werden. Wird die Steptime zu kurz gewählt, so erreicht der Servo sein Ziel nicht. 

## **Installation:**

Um diese Klasse verwenden zu können, muss diese Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:**

Zur Verwendung siehe die Beispiele:
* multiple.ino
* ServoArmController.ino
* simple.ino
* simple_with_range.ino
* DetermineMaxAngle.ino<br />
<br />

**Erläuterung zur Klasse**

Einbinden der Library:
```Arduino
#include <thk_servo_controller.h>
```
<br />

**Instanziieren eines Servo-Objektes:**

```Arduino
thk_ServoController servo(uint8_t aPin, uint32_t aStepTime, int16_t aPos);
```
Bei der Instanziierung eines Servo-Objektes wird der PWM-Pin, die Zeit für einen Stellvorgang und eine Ausgangsposition übergeben. Es gibt weitere überladene Konstruktoren mit weniger Parametern, falls diese nicht benötigt werden.

```Arduino
thk_ServoController servo(uint8_t aPin, int16_t aPos, int16_t aMaxAngle, int16_t aMinAngle);
```
Bei der Instanziierung eines Servo-Objektes wird der PWM-Pin, eine Ausgangsposition und der maximale und minimale anfahrbahre Winkel übergeben. 

`aPin`: Digitaler Pin an dem der Servo angeschlossen ist.
`aPos`: Start position die der Servo nach Initialisierung anfahren soll.
`aMaxAngle`: Größter Winkel der angefahren werden kann.
`aMinAngle`: Kleinster Winkel der angefahren werden kann.<br />
<br />

**Erläuterung zu den Funktionen**

`servo.begin()`: Mit dieser Funktion wird der Servomotor initialisiert und fährt die bei der Instanziierung übergebene Start Position an

`servo.find_max_angle()`:
Mit dieser Funktion kann die maximale Auslenkung über den Seriellen Monitor manuell ermittelt werden.

`servo.write_for_process(int16_t aPos)`:
Diese Funktion setzt den Zielwinkel, der dann mit process() in Schritten erreicht wird. 

`servo.process()`:
Diese Funktion führt die Servobewegung in Schritten durch und muss deshalb im loop() kontinuierlich ausgeführt werden. Jeder Schritt hat die gleiche Dauer.

`uint8_t isBusy`:
Ein Flag zum Lesen. isBusy zeigt an ob derzeit ein Zielwinkel angefahren wird.

`servo.write(int16_t aPos)`:
Diese Funktion ruft direkt write() der Servo-Bibliothek. Der Servo fährt mit max. Geschwindigkeit auf den Zielwinkel.

`servo.turn(int16_t aPos)`:
Diese Funktion prüft zunächst ob der übergebene Winkel in den zuvor definiertem Bereich liegt und ruft dann write() der Servo-Bibliothek auf. Sollte der übergebene Winkel außerhalb des Bereiches liegen, so wird dieser auf die nächstgelegenere Grenze gesetzt. Danach fährt der Servo mit max. Geschwindigkeit auf den Zielwinkel.<br />
<br />

## **Anmerkungen:**

 **Klasse: thk_ServoList**
  
 Diese Klasse kann bis zu 8 ServoController als Liste halten. Die Methode process()
 jedes thk_ServoControllers wird dann durch Rufen der Methode process() der ServoList 
 ausgeführt.