/*
 * Testprogramm: 
 * Mit Hilfe des seriellen Monitors können die maximal möglichen Auslenkungen manuell ermittelt werden.
 */

#include "thk_servo_controller.h"

#define SERVO_PIN 11
#define ANGLE_MAX 180
#define ANGLE_MIN 0
#define START_POS 90
thk_ServoController servo(SERVO_PIN, START_POS, ANGLE_MAX, ANGLE_MIN);

void setup()
{
  Serial.begin(115200);
  servo.begin();
}

void loop()
{
  servo.find_max_angle();
}