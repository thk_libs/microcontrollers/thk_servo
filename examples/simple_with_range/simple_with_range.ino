/*
 * Testprogramm: 
 * Der Servo soll beim Drehen nicht die vordefinierten Grenzen überschreiten.
 * Befindet sich der Servo auf der unteren Grenze, bewegt sich der Servo auf Schrittweise in Richtung der oberen Grenze und umgekehrt.
 */

#include "thk_servo_controller.h"

#define SERVO_PIN 12
#define Angle_max 170
#define Angle_min 16
#define Start_pos 90
thk_ServoController servo(SERVO_PIN, Start_pos, Angle_max, Angle_min);

const uint16_t  updateInterval = 100; //30
unsigned long lastUpdate;
int direction = 0;
uint16_t position = 0;

void setup()
{
  Serial.begin(115200);
  servo.begin();
  servo.turn(80);
}

void loop()
{
  if((millis() - lastUpdate) > updateInterval){
        if (direction == 0){
            position = position + 11;
            if (position >= 180){
                direction = 1;
            }
        } else {
            position = position - 11;
            if (position <= 0){
                direction = 0;
            }
        }
        
        lastUpdate = millis();
        servo.turn(position);
    }
    
}