// Testprogramm: 
// In der Schrittlänge von 5s sollen sich 2 Servos mit unterschiedlicher Geschwindigkeit bewegen.
// Der erste Servo soll 1s für seine Bewegung verwenden, der 2. Servo 2s.

#include "thk_servo_list.h"

thk_ServoList sl;

void setup()
{
  Serial.begin(115200);
  sl.addServo(3, 1000);
  sl.addServo(5, 2000);
}

void loop()
{ 
  static uint32_t t = 0;
  static byte p = 0;
  if ((millis() - t) > 5000) //Schrittlänge = 2s
  {
    t = millis();
    if (p == 0)
    {
      Serial.println("Write for process: 120");
      sl.write_for_process(0, 120);
      sl.write_for_process(1, 120);
      p = 120;
    }
    else if (p == 120)
    {
      Serial.println("Write for process: 0");
      sl.write_for_process(0, 0);
      sl.write_for_process(1, 0);
      p = 0;
    }
  }
  sl.process();
}