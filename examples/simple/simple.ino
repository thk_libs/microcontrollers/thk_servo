// Testprogramm: 
// In der Schrittlänge von 5s soll sich der Servo 1s lang von 120 Grad auf 0 bewegen.
// Befindet sich der Servo auf 0 Grad und die 5s sind vorbei, bewegt sich der Servo auf Winkel 120 und benötigt dafür wieder 1s.
// Dann wiederholt sich der Vorgang.

#include "thk_servo_controller.h"

#define SERVO_PIN 9

thk_ServoController servo(SERVO_PIN, 1000, 0); //Dauer einer Bewegung 1000ms = 1s, Anfangswinkel 0 Grad

void setup()
{
  Serial.begin(115200);
  servo.begin();
}

void loop()
{
  static uint32_t t=0;
  static byte p=0;
  if ((millis()-t)>5000) //Schrittlänge = 5s
  {
    t=millis();
    if (p==0)
    {
      Serial.println("Write for process: 120");
      servo.write_for_process(120);
      p=120;
    }
    else if (p==120)
    {
      Serial.println("Write for process: 0");
      servo.write_for_process(0);
      p=0;
    }
  }
  servo.process();
}