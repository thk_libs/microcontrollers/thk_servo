/*
Funktion:
=========
Dieser Sketch enthält die Firmware eines Servoarms mit 4 Servos. 
Über Potentiometer können Positionen geteacht werden. 
Diese Positionen werden dann automatisch abgefahren.

benötigt die servo_list/servo_controller.h und TT_Button.h !


Hardwareaufbau:
===============
Poti1		A0	ANALOG INPUTs
Poti2		A1
Poti3		A2
Poti4		A3
Servo1	D3	PWM OUTPUTs
Servo2	D5
Servo3	D6
Servo4	D9
Taster	D2 	DIGITAL INPUT
LED1		D4	DIGITAL OUTPUTs
LED2		D7
LED3		D8
LED4		D10
LED5		D11
LED6		D12
LED7		A5
LED8		A4
Buzzer	D13

*/

#include "thk_servo_list.h"
#include "button_controller.h"

#define LED_COUNT 8
#define SERVO_COUNT 4
#define TONE_LENGTH 90 //ms
#define LED_TIME 100   //ms
#define POTI_READ_TIME 20 //ms
#define SERVO_UPDATE_TIME 20 //ms
#define SERVO_STEP_TIME 500  //ms
#define SERVO_STEP_MAX 100

const uint8_t servo_pins[SERVO_COUNT] = {3, 5, 6, 9};
const uint8_t poti_pins[SERVO_COUNT] = {A0, A1, A2, A3};
const uint8_t led_pins[LED_COUNT] = {4, 7, 8, 10, 11, 12, A4, A5};
const uint8_t buzzer_pin = 13;
const uint8_t button_pin = 2;

thk_ServoList servos;
ButtonController bn(button_pin, &shortPress, &longPress, DETECT_GND);
uint8_t potis[SERVO_COUNT];
enum states {PRG, RUN};
uint8_t state = PRG;
uint8_t steps[SERVO_STEP_MAX][SERVO_COUNT];
uint8_t stp = 0;
uint8_t stp_cnt = 0;


// Button-Callback Funktion: Abspeichern der Servopositionen
void shortPress()
{
  String s;
  switch (state)
  {
    case (PRG):
      buz();
      s = "{";
      for (uint8_t i = 0; i < SERVO_COUNT; i++)
      {
        steps[stp][i] = potis[i];
        s += String(potis[i]);
        if (i < 3) s += ", ";
      }
      s += "},";
      Serial.println(s);

      stp = (stp + 1) % SERVO_STEP_MAX;
      stp_cnt = max(stp_cnt, stp);
      break;
    case (RUN):
      break;
  }
}

// Button-Callback Funktion: Umschalten von: Teachen (PRG) -und- Bewegen (RUN)
void longPress()
{
  buz();
  state = (state + 1) % 2;
  Serial.print(F("Statusänderung auf "));
  Serial.println(state);

  switch (state)
  {
    case (PRG):
      stp = 0;
      stp_cnt = 0;
      shortPress();
      break;
    case (RUN):
      stp = 0;
      break;
  }
}

void poti_process()
{
  static uint32_t t;

  if (millis() - t > POTI_READ_TIME)
  {
    switch (state)
    {
      case (PRG):
        // Potentiometerwerte werden gespeichert
        for (uint8_t i = 0; i < SERVO_COUNT; i++)
        {
          uint8_t v = map(analogRead(poti_pins[i]), 0, 1023, 0, 180);
          if (i % 2) v = 180 - v;
          potis[i] = v;
        }

        break;
      case (RUN):
        // Keine Verarbeitung der Potentiometer
        break;
    }
    t = millis();
  }
}


void servo_step_process()
{
  static uint32_t t;
  switch (state)
  {
    case (PRG):
      if (millis() - t > SERVO_UPDATE_TIME)
      {
        for (uint8_t i = 0; i < SERVO_COUNT; i++)
          servos.write(i, potis[i]);

        t = millis();
      }
      break;

    case (RUN):
      if (millis() - t > SERVO_STEP_TIME)
      {
        String s = "Step: " + String(stp + 1) + "/" + String(stp_cnt) + "\t{";

        for (uint8_t i = 0; i < SERVO_COUNT; i++)
        {
          uint8_t v = steps[stp][i];
          servos.write_for_process(i, v);
          s += String(v);
          if (i < SERVO_COUNT - 1) s += ", ";
        }
        s += "}";
        Serial.println(s);

        stp = (stp + 1) % stp_cnt;
        t = millis();
      }
      break;
  }
}

void led_process()
{
  static uint32_t t;
  static bool b = 0;
  if (millis() - t > LED_TIME)
  {
    switch (state)
    {
      case (PRG):
        for (uint8_t i = 0; i < LED_COUNT; i++)
          digitalWrite(led_pins[i], HIGH);
        break;
      case (RUN):
        for (uint8_t i = 0; i < LED_COUNT; i++)
          digitalWrite(led_pins[i], (i + b) % 2);
        b = !b;
    }

    t = millis();
  }
}

void buz()
{
  digitalWrite(buzzer_pin, HIGH);
}

void buzzer_process()
// Diese Funktion schaltet einen ggf. eingeschalteten Buzzerton nach
// einer global definierten Zeit wieder ab
{
  static uint32_t t;
  static bool b = 0;
  if ((b == 0) && (digitalRead(buzzer_pin) == HIGH))  // Flankenwechsel "Einschalten"
  {
    b = 1;                                            // Ton-Flag auf EIN setzen
    t = millis();                                     // Zeit-Flag auf jetzt setzen
  }
  if (millis() - t > TONE_LENGTH)                     // nach xx ms ...
  {
    digitalWrite(buzzer_pin, LOW);                    // Ton ausschalten
    b = 0;                                            // Ton-Flag auf AUS setzen
  }
}




void setup()
{
  Serial.begin(115200);
  Serial.println(F("Servoarm Control"));

  pinMode(button_pin, INPUT_PULLUP);
  pinMode(buzzer_pin, OUTPUT);
  digitalWrite(buzzer_pin, LOW);

  for (uint8_t i = 0; i < LED_COUNT; i++)
    pinMode(led_pins[i], OUTPUT);

  for (uint8_t i = 0; i < SERVO_COUNT; i++)
    servos.addServo(servo_pins[i], 500, 90);

}

void loop()
{
  bn.process();

  poti_process();

  servo_step_process();

  servos.process();

  led_process();

  buzzer_process();
}