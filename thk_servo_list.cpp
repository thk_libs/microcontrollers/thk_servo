#include <thk_servo_list.h>

uint8_t thk_ServoList::countvalue(0);

void thk_ServoList::addServo(uint8_t aPin, uint32_t aStepTime, int16_t aPos)
{
  servos[countvalue] = new thk_ServoController(aPin, aStepTime, aPos);
  servos[countvalue]->begin();
  countvalue = (countvalue + 1)% MAXSERVOS;
};

void thk_ServoList::addServo(uint8_t aPin, uint32_t aStepTime)
{
  servos[countvalue] = new thk_ServoController(aPin, aStepTime);
  servos[countvalue]->begin();
  countvalue = (countvalue + 1)% MAXSERVOS;
};

void thk_ServoList::calibrateServo(uint8_t servo_no, int16_t aLeft, int16_t aMid, int16_t aRight)
{
  if(servo_no <= countvalue)
    servos[servo_no]->calibrate(aLeft, aMid, aRight);
};

void thk_ServoList::write(uint8_t servo_no, int16_t aPos)
{
  servos[servo_no]->write(aPos);
};

void thk_ServoList::write_for_process(uint8_t servo_no, int16_t aPos)
{
  servos[servo_no]->write_for_process(aPos);
};

void thk_ServoList::process()
{
  static long t_process;

  if (millis() - t_process > 5)
  {
    for (byte i=0; i<=countvalue; i++)
      servos[i]->process();	
    t_process=millis();
  }
};